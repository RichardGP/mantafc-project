module.exports = (err, req, res, next) =>{
    const statusCode = err.status || 500;
    const message = err.message || "Internal Server error";

    return res.status(statusCode).send({status:statusCode, message: message});
}