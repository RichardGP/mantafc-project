const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../env_config')
module.exports = function (req, res, next) {
    const token = req.header('Authorization');

    if(!token){
        const error = new Error();
        error.message = "No autorizado";
        error.status = 401;
        throw error;
    }

    let separedToken = token.split(',')[1]  

    if(separedToken == null){
        const error = new Error();
        error.message = "No autorizado";
        error.status = 401;
        throw error;
    }

    jwt.verify(separedToken, JWT_SECRET, (err, decodedToken)=>{
        if(err){
            const error = new Error();
            error.message = "Token invalido";
            error.status = 401;
            throw error;
        }
        
        req.user = decodedToken.user;
        
        next();
    });
}