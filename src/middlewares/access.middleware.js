module.exports = (req, res, next)=>{
    if(!req.user.isAdmin){
        const error = new Error();
        error.message = "No tiene permisos para realizar esta función";
        error.status = 401;
        throw error;
    }

    next();
}