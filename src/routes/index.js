const express = require('express');
require('express-async-errors');
const cors = require('cors');
const morgan = require('morgan');
const helmet = require('helmet');
const path = require('path');
const cronJob = require('node-cron');

const {AuthMiddleware,NotFoundMiddleware, ErrorMiddleware, AccessMiddleware} = require('../middlewares')
const {NewPeriodHelper} = require('../helpers');
const newPeriodHelper = require('../helpers/newPeriod.helper');

module.exports = function({UserRoutes, AuthRoutes, CategoriaRoutes, ControlJugadorRoutes, 
    JugadorRoutes, PosicionRoutes, PartidoJugadoRoutes, PeriodoRoutes, PeriodoService}){
    const apiRoutes = express.Router();
    const router = express.Router();
    
    apiRoutes
        .use(express.json())
        .use(cors())
        .use(morgan('dev'))
        .use(helmet());
    
    cronJob.schedule('1 3 1 January *',async ()=>{
        await newPeriodHelper(PeriodoService);
    });

    apiRoutes.use('/user', [AuthMiddleware], UserRoutes)
            .use('/auth' ,AuthRoutes) 
            .use('/categories',[AuthMiddleware], CategoriaRoutes)
            .use('/test',[AuthMiddleware], ControlJugadorRoutes)
            .use('/player',[AuthMiddleware], JugadorRoutes)
            .use('/position', [AuthMiddleware], PosicionRoutes)
            .use('/game-played', [AuthMiddleware], PartidoJugadoRoutes)
            .use('/period', [AuthMiddleware], PeriodoRoutes);
            

    router.use('/api',apiRoutes)
            .use('/src/uploads', express.static(path.resolve('src/uploads')))
            
    //Middlewares
    router.use(AuthMiddleware, NotFoundMiddleware);
    router.use(AuthMiddleware, ErrorMiddleware);

    
    return router;
};

