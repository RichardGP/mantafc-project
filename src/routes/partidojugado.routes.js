const { Router } = require('express');
const express = require('express');

module.exports = function({PartidoJugadoController}){
    const router = express.Router();

    router.get('/',PartidoJugadoController.getAll);
    router.get('/:id',PartidoJugadoController.getOne);
    router.get('/player/:id',PartidoJugadoController.getPartidoJugadoPorJugador);
    router.post('/create',PartidoJugadoController.create);
    router.patch('/update/:id',PartidoJugadoController.update);

    return router;
}