const express = require('express');

module.exports = function ({CategoriaController}){
    const router = express.Router();

    router.get('/:id', CategoriaController.getOne);
    router.get('/',CategoriaController.getAll);
    router.get('/total/subs', CategoriaController.getTotalByCategory);
    router.get('/total/subs/:id', CategoriaController.getTotalCategoryById);
    
    return router;
}