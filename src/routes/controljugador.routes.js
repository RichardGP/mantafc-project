const express = require('express');

module.exports = function ({ControlJugadorController}){
    const router = express.Router();
    router.get('/:id', ControlJugadorController.getOne);
    router.get('/', ControlJugadorController.getAll);
    //router.get('/player/:id',ControlJugadorController.getTestByPlayer);
    //router.get('/trainer/:id',ControlJugadorController.getTestByTrainer);
    router.post('/create', ControlJugadorController.create);
    router.patch('/update/:id', ControlJugadorController.update);
    
    return router;
}