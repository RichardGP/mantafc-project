const express = require('express');
const {UploadImageMiddleware} = require('../middlewares/index');

module.exports = function ({JugadorController}){
    const router = express.Router();

    router.get('/:id', JugadorController.getOne);
    router.get('/',JugadorController.getAll);
    router.get('/category/:idCategory/period/:idPeriodo', JugadorController.getAllByCategory)
    router.post('/create',UploadImageMiddleware.single('Foto'), JugadorController.create);
    router.patch('/update/:id', UploadImageMiddleware.single('Foto'), JugadorController.update);

    return router;
}