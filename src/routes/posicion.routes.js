const express = require('express');

module.exports = function ({PosicionController}){
    const router = express.Router();

    router.get('/:id', PosicionController.getOne);
    router.get('/',PosicionController.getAll);   
    
    return router;
}