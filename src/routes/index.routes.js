module.exports = {
    AuthRoutes: require('./auth.routes'),
    CategoriaRoutes: require('./categoria.routes'),
    ControlJugadorRoutes:require('./controljugador.routes'),
    JugadorRoutes:require('./jugador.routes'),
    PosicionRoutes:require('./posicion.routes'),
    UserRoutes:require('./user.routes'),
    PartidoJugadoRoutes:require('./partidojugado.routes'),
    PeriodoRoutes:require('./periodo.routes')
    
}