const express = require('express');
const {UploadImageMiddleware} = require('../middlewares');

module.exports = function ({UserController}){
    const router = express.Router();

    
    router.get('/',UserController.getAll);
    router.get('/admin',UserController.getUsersAdmins);
    router.get('/trainer',UserController.getUsersTrainers);
    router.get('/category/:id', UserController.getTrainerByCategoryId);
    router.get('/:id', UserController.getOne);
    router.post('/create',UploadImageMiddleware.single('Foto'),UserController.createUser);
    router.patch('/update/:id', UploadImageMiddleware.single('Foto'), UserController.updateUser);
    
    return router;
}