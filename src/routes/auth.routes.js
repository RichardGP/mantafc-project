const express = require('express');

module.exports = function ({AuthController}){
    const router = express.Router();

    router.post('/signin', AuthController.signIn);

    return router;
}