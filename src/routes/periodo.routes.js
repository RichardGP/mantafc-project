const express = require('express');


module.exports = function({PeriodoController}){
    const router = express.Router();
    
    router.get('/', PeriodoController.getAll);
    router.get('/:id', PeriodoController.getOne);

    return router;
}