const BaseRepository = require('./base.repository');
let _user = null;
let _categoria = null;
let _jugador = null;

module.exports = class UserRepository extends BaseRepository {
    constructor({Usuario, Categoria, Jugador}) {
        super(Usuario);
        _user = Usuario;
        _categoria = Categoria;
        _jugador = Jugador;
    }

    async getOne(_id){        
        return await _user.findAll({where: {id: _id}, include:{model:_jugador}, include:{model:_categoria, as:'categoria', attributes:['id', 'NombreCategoria']}});
    }

    async getByUsername(username){
        return await _user.findAll({where:{Username:username}})
    }

    async getUsersAdmins(){
        return await _user.findAll({where: {esAdmin:true}});
    }

    async getUsersTrainers(){        
        return await _user.findAll({where: {esAdmin:false}});
    }

    async getTrainerByCategoryId(id){        
        return await _user.findAll({where: {idCategoria:id}, attributes:['id', 'Nombre', 'Apellido']});
    }
}
