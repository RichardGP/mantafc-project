module.exports = {
    UserRepository:require('./user.repository'),
    CategoriaRepository:require('./categoria.repository'),
    ControlJugadorRepository:require('./controljugador.repository'),
    JugadorRepository:require('./jugador.repository'),
    PosicionRepository:require('./posicion.repository'),
    PartidoJugadoRepository: require('./partidojugado.repository'),
    PeriodoRepository:require('./periodo.repository')
}