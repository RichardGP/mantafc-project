const BaseRepository = require('./base.repository');
let _periodo = null;

module.exports = class PeriodoRepository extends BaseRepository {
    constructor({Periodo}) {
        super(Periodo);    
        _periodo = Periodo;
    }

    async getPeriodByYear(year){
        let periodo = await _periodo.findAll({where:{Anio:2021}}); 
        return periodo;

    }
}

