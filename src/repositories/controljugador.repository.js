const BaseRepository = require('./base.repository');
let _controlJugador = null;
let _usuario = null;
let _jugador = null;

module.exports = class ControlJugadorRepository extends BaseRepository {

    constructor({ControlJugador, Usuario, Jugador}) {
        super (ControlJugador);
        _controlJugador = ControlJugador;
        _usuario = Usuario;
        _jugador = Jugador;
    }


    async getAll(){
        const  test = await _controlJugador.findAll({include:{model:_usuario, attributes:['id','Username', 'Nombre', 'Apellido']}});
        return test;
    }
}