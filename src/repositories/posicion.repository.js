const BaseRepository = require('./base.repository');
let _posicion = null;

module.exports = class PosicionRepository extends BaseRepository {

    constructor({Posicion}){
        super(Posicion);
        _posicion = Posicion;
    }
}