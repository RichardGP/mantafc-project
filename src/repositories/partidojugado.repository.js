const BaseRepository = require('./base.repository');
let _partidoJugado = null;

module.exports = class PartidoJugadoRepository extends BaseRepository{
    constructor({PartidoJugado}){
        super(PartidoJugado);
        _partidoJugado = PartidoJugado;
    }   

    async getPartidoJugadoPorJugador(id){
        return await _partidoJugado.findAll({where:{idJugador:id}});
    }
    
    
}