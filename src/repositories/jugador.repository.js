const BaseRepository = require('./base.repository');
let _jugador = null;
let _controljugador =null; 
let _usuario = null;
let _categoria = null;
let _posicion = null;
let _partidoJugado = null;

module.exports = class JugadorRepository extends BaseRepository {
    constructor({Jugador, ControlJugador, Usuario, Categoria, Posicion, PartidoJugado}) {
        super (Jugador);
        _jugador = Jugador;
        _controljugador = ControlJugador;
        _usuario = Usuario;
        _categoria = Categoria;
        _posicion = Posicion;
    }

    async getOne(_id){
        return await _jugador.findAll({where: {id: _id}, include:[
            {model:_categoria, as:'categoria', attributes:['id','NombreCategoria']},
            {model:_usuario,as:'entrenador', attributes:['id','Nombre', 'Apellido']},
            {model:_controljugador, 
            attributes:['id', 'Peso', 'Estatura', 'Velocidad','CapacidadSalto','Resistencia', 'CreatedAt'],
            as:'control', include:{model:_usuario, attributes:['id','Username', 'Nombre', 'Apellido']}}
            ,{model:_posicion, as:'posicion', attributes:['id', 'NombrePosicion']}
        
        ]})
    }

    async getAll(){
        return await _jugador.findAll({include:
            [{model:_usuario,as:'entrenador', attributes:['id','Username', 'Nombre', 'Apellido']},
            {model:_posicion, as:'posicion', attributes:['id', 'NombrePosicion']}]
        })
    }

    async getAllByCategory(_id, _idPeriodo){
        return await _jugador.findAll({ where:[{idCategoria:_id},{idPeriodo:_idPeriodo}], 
            include:[{model:_usuario, as: 'entrenador', attributes:['id','Username', 'Nombre', 'Apellido']},
                    {model:_posicion, as:'posicion', attributes:['id', 'NombrePosicion']}
                ]});
    }
}