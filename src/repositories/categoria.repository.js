const BaseRepository = require('./base.repository');
let _categoria = null;
let _user = null;
let _jugador = null;

module.exports = class CategoriaRepository extends BaseRepository {
    constructor({Categoria, Usuario, Jugador}) {
        super (Categoria);
        _categoria = Categoria;
        _user = Usuario;
        _jugador = Jugador;
    }
    
    async getOne(_id){        
        return await _categoria.findAll({where: {id: _id}, include:{model:_user, attributes:['id', 'Username', 'Nombre', 'Apellido', 'Cedula']}});
    }

    async getTotalCategorySub(_idCategoria){
        return await _jugador.count({include:{model:_categoria, as:'categoria'}, where:{idCategoria:_idCategoria}});
    }
}