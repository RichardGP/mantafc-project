
module.exports = class BaseRepository{
    constructor(model) {
        this.model = model;
    }

    async getOne(_id){
        
        return await this.model.findAll({where: {id: _id}})
    }

    async getAll(){
        return await this.model.findAll();
    }

    async create(entity) {
        return await this.model.create(entity);
    }

    async update(_id, entity){
        return await this.model.update(entity, {where:{id:_id}});
    }

    async delete(_id){
        return await this.model.delete({where:{id:_id}});
    }
}