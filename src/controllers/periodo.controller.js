const BaseController = require('./base.controller');
let _periodoService = null;

module.exports = class PeriodoController extends BaseController{
    constructor({PeriodoService}){
        super(PeriodoService);
        _periodoService = PeriodoService;
    }
}
