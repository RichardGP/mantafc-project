const BaseController = require('./base.controller');
let _categoriaService = null;

module.exports = class CategoriaController extends BaseController {
    constructor({CategoriaService}) {
        super(CategoriaService);
        _categoriaService = CategoriaService;
        
    }
    

    async getTotalByCategory(req, res){
        
        const total = await _categoriaService.getTotalByCategory();       
        res.send({total});
    }

    async getTotalCategoryById(req, res){
        const {id} = req.params;
        const total = await _categoriaService.getTotalCategoryById(id);       
        res.send(total);
    }
}