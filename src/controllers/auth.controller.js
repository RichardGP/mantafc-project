let _authService = null;

module.exports = class AuthController{
    constructor({AuthService}) {
        _authService = AuthService;
    }

    async signIn(req, res) {
        const {body} = req;
        const creds = await _authService.signIn(body);
        return res.status(200).send(creds);
    }

}