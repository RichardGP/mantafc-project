module.exports = {
    UserController: require('./user.controller'),
    AuthController: require('./auth.controller'),
    CategoriaController: require('./categoria.controller'),
    ControlJugadorController: require('./controljugador.controller'),
    JugadorController: require('./jugador.controller'),
    PosicionController: require('./posicion.controller'),
    PartidoJugadoController: require('./partidojugado.controller'),
    PeriodoController:require('./periodo.controller')
}