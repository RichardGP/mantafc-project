const BaseController = require('./base.controller');
let _userService = null;

module.exports = class UserController extends BaseController {

    constructor({UserService}) {
        super(UserService);
        _userService = UserService;
    }
    
    
    async createUser(req, res) {
        const {body, file} = req;
        const createdUser = await _userService.createUser(body, file);
        return res.status(200).send(createdUser);
    }

    async getUsersAdmins(req, res){
        const usersAdmins = await _userService.getUsersAdmins();
        return res.status(200).send(usersAdmins);
    }

    async getUsersTrainers(req, res){
        
        const usersTrainers = await _userService.getUsersTrainers();        
        return res.status(200).send(usersTrainers);
    }


    async getTrainerByCategoryId(req, res){
        const {id} = req.params;
        const trainerByidCategory = await _userService.getTrainerByCategoryId(id);
        return res.status(200).send(trainerByidCategory[0]);
    }

    async updateUser(req, res){
        const user = req.body;
        const {file}  = req;
        const userId = req.params.id;

        const userUpdated = await _userService.updateUser(userId, user, file);

        return res.status(200).send(userUpdated);
    }
        
}