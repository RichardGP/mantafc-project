const BaseController = require('./base.controller');
let _posicionService = null;

module.exports = class PosicionController extends BaseController {

    constructor({PosicionService}) {
        super(PosicionService);
        _posicionService = PosicionService;
    }
}