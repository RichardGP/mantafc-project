const BaseController = require('./base.controller');
let _partidoJugadoService = null;

module.exports = class PartidoJugadoController extends BaseController{
    constructor({PartidoJugadoService}){
        super(PartidoJugadoService);
        _partidoJugadoService = PartidoJugadoService;
    }

    async getPartidoJugadoPorJugador(req, res){
        const {id} = req.params;
        
        const partidos = await _partidoJugadoService.getPartidoJugadoPorJugador(id);

        res.send(partidos);
    }
    
}
