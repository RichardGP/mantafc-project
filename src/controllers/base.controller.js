module.exports = class BaseController{
    constructor(service) {
        this.service = service;
    }

    getOne = async(req, res) =>{
        const {id} = req.params;
        const result = await this.service.getOne(id);
        res.status(200).send(result[0]);
    }

    getAll = async(req, res) => { 
        const result = await this.service.getAll();
        res.status(200).send(result);
    }

    create = async(req, res) =>{
        const {body} = req;
        const result = await this.service.create(body);
        res.status(200).send(result);
    }
    
    update = async(req, res) =>{
        const {body} = req;
        const {id} = req.params;
        const result = await this.service.update(id, body);
        res.send(result)
    }

    delete = async(req, res) =>{
        const {id} = req.params;
        const result = await this.service.delete(id);
        res.send(result);
    }
}