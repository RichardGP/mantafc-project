const JugadorService = require('../services/jugador.service');
const BaseController = require('./base.controller');
let _jugadorService = null;

module.exports = class JugadorController extends BaseController {

    constructor({JugadorService}) {
        super (JugadorService);
        _jugadorService = JugadorService;
    }

    create = async(req, res)=>{
        const {body, file} = req;
        const player = await _jugadorService.createPlayer(body, file);
        
        res.status(200).send(player);
    }

    update = async(req, res)=>{
        const {body, file} = req;
        const {id} = req.params;
        const player = await _jugadorService.updatePlayer(id, body, file);
        
        res.status(200).send(player);
    }

    async getAllByCategory(req, res){
        const idCategoria = req.params.idCategory;
        const idPeriodo = req.params.idPeriodo;

        const jugadores = await _jugadorService.getAllByCategory(idCategoria, idPeriodo);
       
        return res.status(200).send(jugadores);
    }

    
}