const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {JWT_SECRET} = require('../env_config/index')

const authHelper = {};

authHelper.encryptPassword = async(password) =>{
    salt = await bcrypt.genSalt(10);
    encryptedPassword = await bcrypt.hash(password, salt);
    return encryptedPassword;
}

authHelper.comparePassword = async(password, savedPassword) =>{

    return await bcrypt.compare(password, savedPassword);
}


authHelper.generateToken = (user) =>{
    return jwt.sign({user},JWT_SECRET,{expiresIn:"4h"});
}

module.exports = authHelper;