
module.exports = async (periodoService)=>{
    let anio = parseInt(new Date().getFullYear());
    const result = await periodoService.create({Anio:anio});
    if(result)
        console.log(`Periodo ${anio} Insertado Correctamente`);
}
