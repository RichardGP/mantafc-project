const BaseService = require('./base.service');
let _periodoRepository = null;

module.exports = class  PeriodoService extends BaseService{
    constructor({PeriodoRepository}){
        super(PeriodoRepository);
        _periodoRepository = PeriodoRepository;
    }

    async getPeriodByYear(year){

        if(!year){
            const error = new Error();
            error.message = 'Se debe enviar el año';
            error.status = 400;
        }
        
        return await _periodoRepository.getPeriodByYear(year);
    }
}