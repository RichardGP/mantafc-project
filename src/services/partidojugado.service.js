const BaseService = require('./base.service');
let _partidoJugadoRepository = null;

module.exports = class PartidoJugadoService extends BaseService{
    constructor({PartidoJugadoRepository}){
        super(PartidoJugadoRepository);
        _partidoJugadoRepository = PartidoJugadoRepository;
    }

    async getPartidoJugadoPorJugador(id){
        if(!id){
            const error = new Error();
            error.message = "El id del jugador no existe";
            error.status = 404;
            throw error;
        }

        return await _partidoJugadoRepository.getPartidoJugadoPorJugador(id);
    }

}