const BaseService = require('./base.service');
let _posicionRepository = null;

module.exports = class PosicionService extends BaseService {
    constructor({PosicionRepository}) {
        super(PosicionRepository);
        _posicionRepository = PosicionRepository;        
    }

}