module.exports = {
    UserService:require('./user.service'),
    AuthService:require('./auth.service'),
    CategoriaService:require('./categoria.service'),
    ControlJugadorService:require('./controljugador.service'),
    JugadorService: require('./jugador.service'),
    PosicionService:require('./posicion.service'),
    PartidoJugadoService:require('./partidojugado.service'),
    PeriodoService:require('./periodo.service')
}