module.exports = class BaseService {
    constructor(repository) {
        this.repository = repository;
    }

    async getOne(_id){
        if(!_id){
            const error = new Error();
            error.message = "El id debe ser enviado";
            error.status = 400;
            throw error;
        }

        const entity = await this.repository.getOne(_id);
        if(!entity[0]){
            const error = new Error();
            error.message = "Entidad no encontrada";
            error.status = 404;
            throw error;
        }

        return entity;
    }

    async getAll() {
        return await this.repository.getAll();
    }

    async create(entity) {
        return await this.repository.create(entity);
    }

    async update(_id, entity){
        if(!_id){
            const error = new Error();
            error.message = "El id debe ser enviado";
            error.status = 400;
            throw error;
        }

        return await this.repository.update(_id, entity);
    }

    async delete(_id){
        if(!_id){
            const error = new Error();
            error.message = "El id debe ser enviado";
            error.status = 400;
            throw error;
        }

        return await this.repository.delete(_id);
    }
}