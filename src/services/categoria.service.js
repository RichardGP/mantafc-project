const e = require('express');
const BaseService = require('./base.service');
let _categoriaRepository = null;
let _userService = null;

module.exports = class CategoriaService extends BaseService {
    constructor({CategoriaRepository, UserService}) {
        super(CategoriaRepository);
        _categoriaRepository = CategoriaRepository;
        _userService = UserService;
    }

    async getTotalByCategory(){
        
        const categories = await _categoriaRepository.getAll();

        if(categories.length != 4){
            const error = new Error();
            error.message = "Error al consultar los ids de categoria";
            error.status = 404;
            throw error;
        }
        
        const totalByCategory = [];
        

        for (let ct of categories) {
            let category = {};    

            let subN = await _categoriaRepository.getTotalCategorySub(ct.id);
            category.id = ct.id;
            category.total = subN;
            category.NombreCategoria = ct.NombreCategoria;
            
            totalByCategory.push(category);
        }
        
        

        return totalByCategory;
    }

    async getTotalCategoryById(id){
        if(!id){
            const error = new Error();
            error.message = "Id de categoria invalido";
            error.status = 404;
            throw error;
        }

        const findCategory = await _categoriaRepository.getOne(id);
        const selectedCategory = findCategory[0];

        if(!selectedCategory){
            const error = new Error();
            error.message = "Error al consultar la categoria";
            error.status = 404;
            throw error;
        }

        let category = {};
        category.id = selectedCategory.id;
        category.total = await _categoriaRepository.getTotalCategorySub(id);
        category.NombreCategoria = selectedCategory.NombreCategoria;
        
        return category;
    }


}