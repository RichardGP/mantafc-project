const { AwilixError } = require('awilix');
const fs = require('fs-extra');
const BaseService = require('./base.service');

let _jugadorRepository = null;
let _partidoJugadoService = null;
let _userService = null;
let _periodoService = null;

module.exports = class JugadorService extends BaseService {
    constructor({JugadorRepository, PartidoJugadoService, UserService, PeriodoService}) {
        super(JugadorRepository);
        _jugadorRepository = JugadorRepository;
        _partidoJugadoService = PartidoJugadoService;
        _userService = UserService;
        _periodoService = PeriodoService;
    }


    async getAllByCategory(idCategoria, idPeriodo){
        if(!idCategoria){
            const error  = new Error();
            error.message = "Id de categoria invalido"
            error.status = 404;
            throw error;
        }

        if(!idPeriodo){
            const error  = new Error();
            error.message = "Id de periodo invalido"
            error.status = 404;
            throw error;
        }

        let jugadores = await _jugadorRepository.getAllByCategory(idCategoria, idPeriodo);
       
        for(let jugador of jugadores){
            jugador = await this.addHorasJugadasPorJugador(jugador); 
            
        }


        return jugadores;
    }

    async createPlayer(player, file){
        if(!player){
            const error = new Error();
            error.message = "Jugador Invalido";
            error.status = 404;
            throw error;
        }

        let trainer = await _userService.getTrainerByCategoryId(player.idCategoria);
        player.idUsuario = trainer[0].id;
        
        let year =  new Date().getFullYear()
        let period = await _periodoService.getPeriodByYear(year);
        player.idPeriodo = period[0].id;

        if(file){
            player.Foto = file.path;
        }else{
            player.Foto = null;
        }
        
        return await _jugadorRepository.create(player);
    }

    async updatePlayer(id, player, file){
       
        if(!id){
            const error = new Error();
            error.message = "id de Jugador invalido";
            error.status = 404;
            throw error;
        }

        const currentPlayer =  await _jugadorRepository.getOne(id);

        if(file){
            if(currentPlayer.Foto == null || currentPlayer.Foto == 'null'){
                player.Foto = file.path;
            }else{
                if(file.path != currentPlayer.Foto){                    
                    await fs.unlink(path.resolve(currentPlayer.Foto));
                    player.Foto = file.path;                    
                }                
            }
        }
        

        return await _jugadorRepository.update(id, player);
    }
    
    async addHorasJugadasPorJugador(jugador){
        
        let partidos = await _partidoJugadoService.getPartidoJugadoPorJugador(jugador.id);
        let totalHorasJugadas = 0;
        for(let partido of partidos){
            totalHorasJugadas+= partido.MinutosJugados;
        }
        jugador.dataValues.HorasJugadas = totalHorasJugadas;
        return jugador;
    }
}