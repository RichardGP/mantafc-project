const BaseService = require('./base.service');
let _controlJugadorRepository = null;

module.exports = class ControlJugadorService extends BaseService {
    constructor({ControlJugadorRepository}) {
        super(ControlJugadorRepository);
        _controlJugadorRepository = ControlJugadorRepository;
    }

}