const BaseService = require('./base.service');
const {AuthHelper} = require('../helpers');
const path = require('path');
const fs = require('fs-extra');


let _userRepository = null;

module.exports = class UserService extends BaseService {
    constructor({UserRepository}) {
        super(UserRepository);
        _userRepository = UserRepository;
    }
    
    async getByUsername(username){
        return await _userRepository.getByUsername(username);
    }

    async getUsersAdmins(){
        return await _userRepository.getUsersAdmins();
    }

    async getUsersTrainers(){        
        return await _userRepository.getUsersTrainers();
    }

    async createUser(user, file){
        const {Username, Password} = user;
        
        const userExist = await this.getByUsername(Username);
        
        
        if(userExist[0]){
            const error = new Error();
            error.message = "El usuario ya existe";
            error.status = 401;
            throw error;
        }

        const encryptedPassword = await AuthHelper.encryptPassword(Password);
        user.Password = encryptedPassword;

        if(file){
            user.Foto = file.path;
        }
    
        return await _userRepository.create(user);
    }

    async updateUser(id, user, file){
        if(!id){
            const error = new Error();
            error.message = "El id debe ser enviado";
            error.status = 400;
            throw error;
        }

        const userExist = await this.getOne(id);

        const currentUser = userExist[0];
        
        if(!currentUser){
            const error = new Error();
            error.message = "El usuario no existe";
            error.status = 404;
            throw error;
        }
        
        
        if(file){
            if(currentUser.Foto == null || currentUser.Foto == 'null'){
                user.Foto = file.path;
            }else{
                if(file.path != currentUser.Foto){                    
                    await fs.unlink(path.resolve(currentUser.Foto));
                    user.Foto = file.path;                    
                }                
            }
        }
        

        if(user.Password != 'undefined' && user.Password != null && user.Password != "" && user.Password != 'null'){
            
            const equalsPassword = await AuthHelper.comparePassword(user.Password, currentUser.Password);
            
            if(!equalsPassword){
                const encryptedPassword = await AuthHelper.encryptPassword(user.Password);
                user.Password = encryptedPassword;
            }
        }else{
            
            user.Password = currentUser.Password;
        }

       

        return await _userRepository.update(id, user)
    }
    

    async getAllByCategory(id){
        if(!id){
            const error = new Error();
            error.message = "El id debe ser enviado";
            error.status = 400;
            throw error;
        }
        
        return await _userRepository.getAllByCategory(id);
    }
    
    async getTrainerByCategoryId(id){
        if(!id){
            const error = new Error();
            error.message = "El id de categoria debe ser enviado";
            error.status = 400;
            throw error;
        }       

        return await _userRepository.getTrainerByCategoryId(id);
    }
}