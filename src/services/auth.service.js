const {AuthHelper} = require('../helpers');
_userService = null;

module.exports = class AuthService{
    constructor({UserService}) {
        _userService = UserService;
    }

    async signIn(user) {
        const {Username, Password} = user;
        const userExist = await _userService.getByUsername(Username);
        const currentUser = userExist[0];
        if(!currentUser){
            const error = new Error();
            error.message = "El usuario no existe";
            error.status = 404;
            throw error;
        }

        if(!currentUser.estaActivo){
            const error = new Error();
            error.message = "Usuario inactivo";
            error.status = 401;
            throw error;
        }
        
        const validPassword = await AuthHelper.comparePassword(Password, currentUser.Password);

        if(!validPassword){
            const error = new Error();
            error.message = "La contraseña no es valida";
            error.status = 401;
            throw error;
        }
        const userToEncode = {
            id: currentUser.id,
            Nombre:currentUser.Nombre,
            Apellido:currentUser.Apellido,
            Username: currentUser.Username,
            isAdmin: currentUser.esAdmin,
            estaActivo:currentUser.estaActivo
        }
                
        const token = AuthHelper.generateToken(userToEncode);
            
        return {token, user:userToEncode}
    }

    
}