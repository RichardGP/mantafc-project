/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const ControlJugador = sequelize.define('controljugador', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Peso: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false
    },
    Estatura: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false
    },
    Velocidad: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false
    },
    CapacidadSalto: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false  
    },
    Resistencia: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false
    },
    idJugador: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'jugador',
        key: 'id'
      }
    },
    idUsuario: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'usuario',
        key: 'id'
      }
    }
  }, {
    tableName: 'controljugador'
  });
  ControlJugador.associate = function(models) {
    ControlJugador.belongsTo(models.usuario, {foreignKey:'idUsuario'});
    
  }

  
  return ControlJugador;
};
