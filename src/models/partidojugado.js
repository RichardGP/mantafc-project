module.exports = function(sequelize, DataTypes) {
    const PartidoJugado = sequelize.define('partidojugado', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      NombrePartido:{
        type: DataTypes.STRING(200),
        allowNull:false
      },
      MinutosJugados: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      idJugador: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'jugador',
          key: 'id'
        }
      },
      FechaPartido: {
        type: DataTypes.DATEONLY,
        allowNull: false
      }
    }, {
      tableName: 'partidojugado'
    });
    PartidoJugado.associate = function(models) {
        PartidoJugado.belongsTo(models.jugador, {foreignKey:'idJugador', as:'partidosjugados'}); 
    }
    return PartidoJugado;
}