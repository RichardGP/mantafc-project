/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Posicion = sequelize.define('posicion', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    NombrePosicion: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'posicion'
  });

  Posicion.associate = function(models) {
    Posicion.hasMany(models.jugador, {foreignKey:'idPosicion', as:'posicion'});
  }
  return Posicion;
};
