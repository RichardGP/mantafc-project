/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Usuario = sequelize.define('usuario', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Username: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    Nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Apellido: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Cedula: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    Foto: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    esAdmin: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    estaActivo: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    idCategoria: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'categoria',
        key: 'id'
      }
    }
  }, {
    tableName: 'usuario'
  });

  Usuario.associate = function(models) {
    Usuario.hasMany(models.controljugador, {foreignKey:'idUsuario'});
    Usuario.hasMany(models.jugador, {as:'entrenador', foreignKey:'idUsuario'});
    Usuario.belongsTo(models.categoria, {as:'categoria',foreignKey:'idCategoria'});
    
  }

  return Usuario;
};
