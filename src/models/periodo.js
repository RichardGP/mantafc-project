/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Periodo = sequelize.define('periodo', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Anio: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'periodo'
  });

  Periodo.associate = function(models) {
    Periodo.hasMany(models.jugador, {foreignKey:'idPeriodo', as:'periodo'});
  }
  return Periodo;
};
