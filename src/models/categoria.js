/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const Categoria = sequelize.define('categoria', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    NombreCategoria: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'categoria'
  });
  Categoria.associate = function(models) {
    Categoria.hasMany(models.usuario, {foreignKey:'idCategoria'});
    Categoria.hasMany(models.jugador, {foreignKey:'idCategoria'});
    
  }


  return Categoria;
};
