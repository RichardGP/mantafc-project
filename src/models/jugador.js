//const {Model} = require('sequelize');

module.exports = function(sequelize, DataTypes) {
  const Jugador = sequelize.define('jugador', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Nombres: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Apellidos: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Cedula: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    Foto: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    FechaNacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    Lesion: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    idCategoria: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'categoria',
        key: 'id'
      }
    },
    idPosicion: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'posicion',
        key: 'id'
      }
    },
    idPeriodo: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'periodo',
        key: 'id'
      }
    }
  }, {
    tableName: 'jugador'
  });
  
  Jugador.associate = function(models) {
    Jugador.hasMany(models.controljugador, {foreignKey:'idJugador', as:'control'});
    Jugador.hasMany(models.partidojugado, {foreignKey:'idJugador', as:'partidosjugados'});
    Jugador.belongsTo(models.usuario, {as: 'entrenador', foreignKey:'idUsuario'});
    Jugador.belongsTo(models.categoria, {as:'categoria',foreignKey:'idCategoria'});
    Jugador.belongsTo(models.posicion, {as: 'posicion', foreignKey:'idPosicion'});
    Jugador.belongsTo(models.periodo, {as: 'periodo', foreignKey:'idPeriodo'});
  }

  return Jugador;
};
