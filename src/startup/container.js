const {createContainer, asClass, asFunction, asValue} = require('awilix');

const container = createContainer();

//Config
const config = require('../env_config');
const Router = require('../routes/index');

//{Usuario}
//Repositories
const {UserRepository, CategoriaRepository, JugadorRepository, 
    PosicionRepository, ControlJugadorRepository, PartidoJugadoRepository, 
    PeriodoRepository} = require('../repositories');

//Services
const {UserService, AuthService, CategoriaService, JugadorService, 
    PosicionService,  ControlJugadorService, PartidoJugadoService,
    PeriodoService} = require('../services');

//Controllers
const {UserController, AuthController, CategoriaController, JugadorController, PosicionController, 
    ControlJugadorController, PartidoJugadoController, PeriodoController} = require('../controllers');

//Routes
const {UserRoutes, AuthRoutes, CategoriaRoutes, ControlJugadorRoutes, JugadorRoutes, PosicionRoutes, 
     PartidoJugadoRoutes, PeriodoRoutes} = require('../routes/index.routes');

//Models
const {usuario, posicion, jugador, controljugador, categoria, partidojugado, periodo} = require('../models');



container.register({
    //Configuración
    config: asValue(config),
    router: asFunction(Router).singleton()
}).register({
    //Models
    Usuario:asValue(usuario),
    Jugador:asValue(jugador),
    Posicion:asValue(posicion),
    ControlJugador:asValue(controljugador),
    Categoria:asValue(categoria),
    PartidoJugado:asValue(partidojugado),
    Periodo:asValue(periodo)
}).register({
    //Servicios
    UserService:asClass(UserService).singleton(),
    AuthService:asClass(AuthService).singleton(),
    CategoriaService:asClass(CategoriaService).singleton(), 
    JugadorService:asClass(JugadorService).singleton(), 
    PosicionService:asClass(PosicionService).singleton(), 
    ControlJugadorService:asClass(ControlJugadorService).singleton(),
    PartidoJugadoService:asClass(PartidoJugadoService).singleton(),
    PeriodoService:asClass(PeriodoService).singleton()
}).register({
    //Controladores
    UserController:asClass(UserController.bind(UserController)).singleton(),
    AuthController:asClass(AuthController.bind(AuthController)).singleton(),
    CategoriaController:asClass(CategoriaController.bind(CategoriaController)).singleton(), 
    JugadorController:asClass(JugadorController.bind(JugadorController)).singleton(), 
    PosicionController:asClass(PosicionController.bind(PosicionController)).singleton(),  
    ControlJugadorController:asClass(ControlJugadorController.bind(ControlJugadorController)).singleton(),
    PartidoJugadoController:asClass(PartidoJugadoController).singleton(),
    PeriodoController:asClass(PeriodoController.bind(PeriodoController)).singleton()

}).register({
    //Rutas
    UserRoutes:asFunction(UserRoutes).singleton(),
    AuthRoutes:asFunction(AuthRoutes).singleton(),
    CategoriaRoutes:asFunction(CategoriaRoutes).singleton(),
    ControlJugadorRoutes:asFunction(ControlJugadorRoutes).singleton(),
    JugadorRoutes:asFunction(JugadorRoutes).singleton(),
    PosicionRoutes:asFunction(PosicionRoutes).singleton(),
    PartidoJugadoRoutes:asFunction(PartidoJugadoRoutes),
    PeriodoRoutes:asFunction(PeriodoRoutes)
}).register({
    //Repositorios
    UserRepository:asClass(UserRepository).singleton(),
    CategoriaRepository:asClass(CategoriaRepository).singleton(),
    JugadorRepository:asClass(JugadorRepository).singleton(),
    PosicionRepository:asClass(PosicionRepository).singleton(),
    ControlJugadorRepository:asClass(ControlJugadorRepository).singleton(),
    PartidoJugadoRepository:asClass(PartidoJugadoRepository).singleton(),
    PeriodoRepository:asClass(PeriodoRepository).singleton()
});

module.exports = container;
