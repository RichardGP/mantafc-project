const express = require('express');
const container = require('./container');
const {PORT} = container.resolve('config');
const {APPLICATION_NAME} = container.resolve('config');
const router = container.resolve('router');


const app = express().use(router);


app.listen(PORT, ()=>{
    console.log(`${APPLICATION_NAME} on port ${PORT}`);
});